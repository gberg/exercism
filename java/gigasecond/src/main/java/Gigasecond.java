import java.time.LocalDate;
import java.time.LocalDateTime;
import java.lang.Math;

class Gigasecond {
    //int gigaDays = (int) Math.pow(10,9) / 60 / 60 / 24;
    int gigasecond = (int) Math.pow(10,9);
    LocalDateTime giga;

    Gigasecond(LocalDate birthDate) {
        giga = birthDate.atStartOfDay();
    }

    Gigasecond(LocalDateTime birthDateTime) {
        giga = birthDateTime;
    }

    LocalDateTime getDate() {
        return giga.plusSeconds(gigasecond);
    }
}
