import java.util.Map;
import java.util.HashMap;

public class RnaTranscription {
    public String transcribe(String dnaStrand) {
		Map<Character, Character> map = new HashMap<>();
		map.put('G', 'C');
		map.put('C', 'G');
		map.put('T', 'A');
		map.put('A', 'U');

		StringBuilder rnaStrand = new StringBuilder("");
		for(int i = 0; i < dnaStrand.length(); i++) {
			Character c = new Character(dnaStrand.charAt(i));
			if (map.containsKey(c)) {
				rnaStrand.append(map.get(c));
			} else {
				return "";
			}
		}

		return rnaStrand.toString();
    }
}
