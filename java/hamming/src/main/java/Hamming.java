public class Hamming {
	int dist = 0;
    Hamming(String leftStrand, String rightStrand) throws IllegalArgumentException {
		if (rightStrand.length() != leftStrand.length()) {
			throw new IllegalArgumentException("args must be same length");
		}
		for(int i = 0; i < leftStrand.length(); i++) {
			if ((leftStrand.charAt(i) ^ rightStrand.charAt(i)) != 0) {
				dist++;
			}
		}

    }

    int getHammingDistance() {
		return dist;	
    }

}
