public class HelloWorld {
    public static String hello(String name) {
		String s = "Hello, ";
		if (name == null || name.isEmpty()) {
			s += "World!";
		} else {
			s += name + "!";
		}
		return s;
    }
}
