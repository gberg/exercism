public class PangramChecker {

	public static void main(String[] args) {
		PangramChecker pc = new PangramChecker();
		pc.isPangram("hellowhatisthisnonsense");
	}

    public boolean isPangram(String input) {
		boolean[] alpha = new boolean[26];

		for (int i = 0; i < input.length(); i++) {
			int val = input.charAt(i) ;
			if (val >= 97 && val < 123) {
				alpha[input.charAt(i) - 'a'] = true;
			} else if (val >= 65 && val < 91) {
				alpha[input.charAt(i) - 'A'] = true;
			}
		}
		
		for (int i = 0; i < alpha.length; i++) {
			if (alpha[i] == false) {
				return false;
			}
		}
		
		return true;
	}
}
