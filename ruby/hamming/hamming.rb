class Hamming
  def self.compute(left, right)
    if left.length != right.length 
      raise ArgumentError, 'Strings must be of same length'
    end
  
    distance = 0
    for i in 0..left.length
      if left[i] != right[i]
        distance += 1
      end
    end

    return distance

  end
end
