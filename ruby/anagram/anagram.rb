class Anagram

  def initialize(word)
    @original = word.downcase
    @given = @original.chars.sort
  end

  def match(words)
      words.select { |word| anagram?(word) && !original_word?(word) } 
  end

  def original_word?(word)
    word.downcase.eql?(@original)
  end

  def anagram?(word)
    word.downcase.chars.sort == @given
  end
    

end

module BookKeeping 
  VERSION = 2
end
