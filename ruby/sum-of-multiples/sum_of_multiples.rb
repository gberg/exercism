class SumOfMultiples
  def initialize(*p)
    @factors_of = p
  end

  def to(n)
    a = []
    @factors_of.each do |f|
      a += (1...n).select { |i| i % f == 0 }
    end
    a.uniq.sum
  end

end

module BookKeeping
  VERSION = 1
end

