class Grains
  def self.square(n)
    if (n <= 0 || n > 64)
      raise ArgumentError, 'This chessboard has squares 1 to 64'
    end
    2**(n-1)
  end

  def self.total
    2**64 - 1
  end
end

module BookKeeping
  VERSION = 1
end
