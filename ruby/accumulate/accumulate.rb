class Array
  
  def accumulate
    unless block_given?
      return to_enum
    end
    transformed = []
    each do |i|
      transformed << (yield i)
    end
    transformed
  end

end

module BookKeeping
  VERSION = 1
end
