class SpaceAge

  PLANETS =  { 
    :mercury => 0.2408467,
    :venus   => 0.61519726,
    :mars    => 1.8808158,
    :jupiter => 11.862615,
    :saturn  => 29.447498,
    :uranus  => 84.016846,
    :neptune => 164.79132
  }

  def initialize(seconds)
    @seconds = seconds
    @planets = PLANETS
  end

  def on_earth
    @seconds / 31557600.0
  end
 
  def on_mercury
    on_earth / @planets[:mercury]
  end
  
  def on_venus
    on_earth / @planets[:venus]
  end
  
  def on_mars
    on_earth / @planets[:mars]
  end
  
  def on_jupiter
    on_earth / @planets[:jupiter]
  end
  
  def on_saturn
    on_earth / @planets[:saturn]
  end
  
  def on_uranus
    on_earth / @planets[:uranus]
  end
  
  def on_neptune
    on_earth / @planets[:neptune]
  end

end

module BookKeeping
  VERSION = 1
end

