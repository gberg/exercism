class Trinary

  def initialize(tri_number)
    @tri_number = tri_number
  end

  def to_decimal
    trinary = @tri_number.to_i
    decimal = 0
    place   = 0
    while trinary != 0
      if trinary % 10 > 2 
        return 0
      end
      decimal += (trinary % 10) * 3 ** place
      trinary /= 10
      place += 1
    end
    decimal
  end

end

module BookKeeping
  VERSION = 1
end
