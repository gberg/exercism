require 'benchmark'
class Trinary

  def initialize(tri_number)
    @tri_number = tri_number.gsub(/\n/,'');
  end

  def to_decimal
    return 0 unless @tri_number =~ /^[012]+$/
    @tri_number.chars.reverse.each_with_index
               .map { |tri, i| tri.to_i * 3**i }
               .sum 
  end

end

module BookKeeping
  VERSION = 1
end

