class Phrase
  def initialize(phrase)
    @phrase = phrase
  end

  def word_count
    word_to_count = Hash.new
    words = @phrase.downcase.split
    if words.length == 1
      words = @phrase.downcase.split(',')
    end
    words.each do |word|
      word.gsub!(/[^0-9a-z']/, '')
      word.gsub!(/\A'|'\Z/, '') # strip quotes
      if word_to_count.has_key?(word)
        word_to_count[word] += 1
      else
        word_to_count[word] = 1
      end
    end
    word_to_count
  end
end

module BookKeeping
  VERSION = 1
end
