class RunLengthEncoding
  def self.encode(input)
    output = ''
    pointer = 0
    while pointer  < input.length
      count = 1;
      while input[pointer] == input[pointer + 1]
        count += 1
        pointer += 1
      end
      if count != 1 
        output << count.to_s
      end
      output << input[pointer]
      pointer += 1
    end
    output
  end

  def self.decode(input)
    output = ''
    pointer = 0
    while pointer < input.length
      count_string = ''
      count = 0
      if input[pointer] =~ /[A-Za-z ]/
        output << input[pointer]
      else
        while input[pointer] =~ /[0-9]/
          count_string << input[pointer]
          pointer += 1
        end
        count = count_string.to_i
        while count > 0
          output << input[pointer]
          count -= 1
        end
      end
      pointer += 1
    end
    output
  end
end

module BookKeeping
  VERSION = 3
end

