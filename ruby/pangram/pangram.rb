class Pangram
  def self.pangram?(phrase)
    alphabet_map = ('a'..'z').to_a.map{ |x| [x,false] }.to_h
    phrase.downcase.each_char do |i|
      alphabet_map[i] = true
    end
    alphabet_map.values.all?
  end
end

module BookKeeping
  VERSION = 4
end
