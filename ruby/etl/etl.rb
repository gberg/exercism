class ETL
  def self.transform(old)
    new_scores = {}
    old.each do |k,v|
      v.each { |letter| new_scores[letter.downcase!] = k }
    end
    new_scores
  end
end

module BookKeeping
  VERSION = 1
end
