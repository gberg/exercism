require 'prime'
class PrimeFactors
  def self.for(number)
    prime_factors = Prime.take(Math.sqrt(number + 1))
    a = []
    prime_factors.each do |p|
      while number % p == 0
        a.push(p)
        number /= p
      end
    end
    a
  end
end

