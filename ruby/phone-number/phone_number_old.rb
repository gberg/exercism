class PhoneNumber
  def self.clean(phone_number)
    phone_number = phone_number.gsub(/\D/, '')
    if phone_number.nil?
      return nil
    end
    
    len = phone_number.length 
    if len < 10 || len > 11 || (len  == 11 && !phone_number[0].eql?('1'))
      return nil
    end
    
    if len == 11 && phone_number[0].eql?('1')
      phone_number.reverse!.chop!.reverse!
    end
    
    if phone_number[0].to_i < 2 || phone_number[3].to_i < 2
      return nil
    end

    phone_number
  end
end

module BookKeeping
  VERSION = 2
end

