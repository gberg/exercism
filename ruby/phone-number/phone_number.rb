class PhoneNumber
  def self.clean(phone_number)
    phone_number = phone_number.gsub(/\D/, '')
    
    if phone_number.length == 11 && phone_number[0].eql?('1')
      phone_number[0] = ''
    end    
   
    unless phone_number =~ /^[2-9]\d{2}[2-9]\d{6}$/
      return nil
    end
   
    phone_number
  end
end

module BookKeeping
  VERSION = 2
end

