class Binary
  def self.to_decimal(binary_string)
    if binary_string =~ /[^01]+/
      raise ArgumentError, 'binary numbers can contain only 1\'s and 0\'s'
    end
    decimal_number = 0
    place = binary_string.length - 1
    binary_string.each_char do |c|
      decimal_number += c.to_i * 2**place
      place -= 1
    end
    decimal_number
  end
end

module BookKeeping
  VERSION = 3
end

