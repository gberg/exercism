class Array
  
  def keep
    unless block_given?
      return to_enum
    end
    kept = []
    each do |i|
      if (yield i)
        kept << i
      end
    end
    kept
  end

  def discard
    keep { |i| ! yield i } 
  end

end

module BookKeeping
  VERSION = 1
end
