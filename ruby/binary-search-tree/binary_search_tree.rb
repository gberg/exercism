class Bst
  attr_reader :data, :left, :right

  def initialize(val)
    @data = val
    @left = nil
    @right = nil
  end

  def insert(val)
    if val <= @data
      @left ? @left.insert(val) : @left = Bst.new(val)
    else
      @right? @right.insert(val) : @right = Bst.new(val)
    end
  end

  def each(&block)
    return to_enum(:each) unless block_given?

    @left.each(&block) unless @left == nil
    yield @data
    @right&.each(&block) unless @right == nil  
  end

end

module BookKeeping
  VERSION = 1
end
