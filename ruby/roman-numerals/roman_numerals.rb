class Integer
  INT_TO_ROMAN = {
    1    => 'I',
    4    => 'IV',
    5    => 'V',
    9    => 'IX',
    10   => 'X',
    40   => 'XL',
    50   => 'L',
    90   => 'XC',
    100  => 'C',
    400  => 'CD',
    500  => 'D',
    900  => 'CM',
    1000 => 'M'
  }
  def to_roman
    roman_numeral = ""
    arabic_numeral = self;
    scalar = 1;
    while arabic_numeral != 0
      digit = arabic_numeral % 10
      roman_digit = ""
      if digit < 4
        while digit != 0
          roman_digit << INT_TO_ROMAN[1 * scalar]
          digit -= 1
        end
      elsif digit == 4
        roman_digit << INT_TO_ROMAN[4 * scalar]
      elsif digit == 9
        roman_digit << INT_TO_ROMAN[9 * scalar]
      elsif
        roman_digit << INT_TO_ROMAN[5 * scalar]
        digit -= 5;
        while digit != 0
          roman_digit << INT_TO_ROMAN[1 * scalar]
          digit -= 1
        end
      end
      arabic_numeral /= 10;
      scalar = (scalar == 1) ? scalar + 9 : scalar * 10
      roman_numeral.prepend(roman_digit)
    end
    roman_numeral
  end
end

module BookKeeping
  VERSION = 2
end
