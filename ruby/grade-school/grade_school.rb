class School
   
  def initialize
    @grades = Hash.new
  end

  def add(student, grade)
    unless @grades.has_key?(grade)
      @grades[grade] = []
    end
    @grades[grade].push(student)
  end

  def students(grade)
    unless @grades.has_key?(grade)
      @grades[grade] = []
    end
    @grades[grade].sort
  end

  def students_by_grade
    @grades.keys.sort.map { |grade| { grade: grade, students: students(grade) } }
  end

end

module BookKeeping
  VERSION = 3
end

