class Series
  
  def initialize(number)
    @number = number.to_s
  end

  def slices(slice_size)
    if slice_size > @number.length
      raise ArgumentError, 'the slicer must be smaller then the slicee'
    end
    
    a = []
    bound = @number.length - slice_size
    for i in (0..bound)  do
      a.push(@number.slice(i, slice_size))
    end
    a
  end

end

