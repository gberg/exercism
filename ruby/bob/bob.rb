class Bob
  def self.hey(remark)
    response = ''
    remark.strip!
    if remark == remark.upcase && remark =~ /[A-Za-z]/
      response << 'Whoa, chill out!'
    elsif remark.end_with?('?')
      response << 'Sure.'
    elsif remark.empty? || remark =~ /\A[^A-Za-z0-9]+\z/
      response << 'Fine. Be that way!'
    else
      response << 'Whatever.'
    end
    response
  end
end

module BookKeeping
  VERSION = 1
end
