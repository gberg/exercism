class Game
  
  class BowlingError < StandardError; end

  def initialize
    @rolls = []
    @roll  = 0
    @frame = 1
    @score = 0
    @last_frame  = 0
    @frame_score = 0
    @last_frame_count = 0
    @is_first_roll = true
    @has_fill_ball = false
  end

  def roll(pins)
    raise Game::BowlingError unless pins >= 0 && pins <= 10 && @frame <= 10
    @frame_score += pins
    raise Game::BowlingError if @frame != 10 && @frame_score > 10
    
    # last frame
    if @frame == 10
      if @last_frame_count == 2
        @has_fill_ball = has_fill_ball?
        raise Game::BowlingError unless @has_fill_ball
      end
      @last_frame_count += 1
      if @last_frame == 0
        @last_frame = @roll
      end
      @rolls[@roll] = pins
      @roll += 1
      return
    end

    if pins == 10 && @is_first_roll
      @rolls[@roll] = 'X'
      @is_first_roll = false
    elsif @frame_score == 10 && !@is_first_roll
      @rolls[@roll] = '/'
    else
      @rolls[@roll] = pins
    end
    if !@is_first_roll
      @frame += 1
      @frame_score = 0
    end
    @is_first_roll = !@is_first_roll
    @roll += 1
  end

  def display
    puts @rolls.inspect
  end

  def has_fill_ball?
    last = @rolls.length - 1
    @rolls[last] == 10 || @rolls[last - 1] == 10 || @rolls[last] + @rolls[last - 1] == 10
  end

  def score
    raise Game::BowlingError unless @frame == 10
    @rolls.each_with_index do |roll, index|
      if index == @last_frame
        last_frame(index)
        break
      end
      if roll == 'X'
        @score += 10
        strike(index)
      elsif roll == '/'
        @score += 10 - @rolls[index - 1]
        spare(index)
      else
        @score += roll
      end
    end
    @score
  end

  def strike(index)
    add_next(index)
    add_next(index + 1)
  end

  def spare(index)
    add_next(index)
  end

  def add_next(index)
    if index < @rolls.length
      if @rolls[index + 1] == 'X'
        @score += 10
      elsif @rolls[index + 1] == '/'
        @score += 10 - @rolls[index]
      else
        add_score(index + 1)
      end
    end
  end

  def add_score(index)
    raise Game::BowlingError unless @rolls[index] != nil && @rolls[index] <= 10 
    @score += @rolls[index]
  end

  def last_frame(index)
    add_score(index)
    add_score(index + 1)

    if @has_fill_ball
      raise Game::BowlingError if @rolls[index + 2] == nil
      if @rolls[index + 1] != 10 && @rolls[index] + @rolls[index + 1] != 10 && @rolls[index + 1] + @rolls[index + 2] > 10
         raise Game::BowlingError
      end
      add_score(index + 2)
    end 
  end

end

module BookKeeping
  VERSION = 3
end

