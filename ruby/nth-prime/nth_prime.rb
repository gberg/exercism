require_relative 'sieve'

class Prime
  def self.nth(n)
    if n == 0
      raise ArgumentError, 'No zeroth prime'
    end
    # lol
    prime_array = Sieve.new(n * 50).primes
    prime_array[n - 1]
  end
end

module BookKeeping
  VERSION = 1
end
