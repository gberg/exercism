class Sieve

  def initialize(num)
    if num < 0
      raise ArgumentError, 'Input must be positive'
    end
    @num = num
  end

  def primes
    if @num < 2
      return []
    end
    values = Array.new(@num + 1, true)
    values[0] = false
    values[1] = false
    (2..Math.sqrt(@num)).each do |i|
      if values[i]
        (i**2..@num).step(i) do |j|
          values[j] = false
        end
      end
    end
    primes = []
    values.each_index do |i|
      if values[i]
        primes.push(i)
      end
    end
    return primes
  end
end

module BookKeeping
  VERSION = 1
end

