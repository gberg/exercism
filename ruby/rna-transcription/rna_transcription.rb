class Complement
  def self.of_dna(dna_strand)
    complements = {'G' => 'C', 'C' => 'G', 'T' => 'A', 'A' => 'U'}
    rna_strand = ""
    dna_strand.each_char do |i|
      if complements.has_key?(i)
        rna_strand << complements[i]
      else 
        return ""
      end
    end
    return rna_strand
  end
end

module BookKeeping
  VERSION = 4 
end
