defmodule NucleotideCount do
  @nucleotides [?A, ?C, ?G, ?T]

  @doc """
  Counts individual nucleotides in a NucleotideCount strand.

  ## Examples

  iex> NucleotideCount.count('AATAA', ?A)
  4

  iex> NucleotideCount.count('AATAA', ?T)
  1
  """
  #@spec count([char], char) :: non_neg_integer
  #def count(strand, nucleotide) do
    #count_tides(strand, nucleotide, 0)
  #end

  @spec count([char], char) :: non_neg_integer
  def count(strand, nucleotide) do
    Enum.count(strand, fn n -> n == nucleotide end) 
  end

  def count_tides([head | tail], nucleotide, accumulator) do
    if head == nucleotide do
      count_tides(tail, nucleotide, accumulator + 1)
    else
      count_tides(tail, nucleotide, accumulator)
    end
  end

  def count_tides([], nucleotide, accumulator) do
    accumulator
  end


  @doc """
  Returns a summary of counts by nucleotide.

  ## Examples

  iex> NucleotideCount.histogram('AATAA')
  %{?A => 4, ?T => 1, ?C => 0, ?G => 0}
  """
  #@spec histogram([char]) :: map
  #def histogram(strand) do
    #build_map(@nucleotides, strand, %{})
  #end

  @spec histogram([char]) :: map
  def histogram(strand) do
    @nucleotides
    |> Enum.map(fn(n) -> {n, count(strand, n)} end)
    |> Map.new
  end
  
  def build_map([head | tail], strand, map) do
    map = Map.put(map, head, count(strand, head))
    build_map(tail, strand, map)
  end

  def build_map([], strand, map) do
    map
  end

end
