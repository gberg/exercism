defmodule RotationalCipher do
  @doc """
  Given a plaintext and amount to shift by, return a rotated string.

  Example:
  iex> RotationalCipher.rotate("Attack at dawn", 13)
  "Nggnpx ng qnja"
  """
  @spec rotate(text :: String.t(), shift :: integer) :: String.t()
  def rotate(text, shift) do
    text
    |> to_charlist
    |> Enum.map(&(shift_char(&1, shift)))
    |> to_string
  end

  defp shift_char(c, shift) do
    cond do
      Enum.member?(?a..?z, c) -> rem(c - ?a + shift, 26) + ?a
      Enum.member?(?A..?Z, c) -> rem(c - ?A + shift, 26) + ?A
      true -> c
    end
  end
end

