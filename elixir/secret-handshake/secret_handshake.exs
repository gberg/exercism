defmodule SecretHandshake do
  use Bitwise
  @key_list ["wink", "double blink", "close your eyes", "jump"]
  @doc """
  Determine the actions of a secret handshake based on the binary
  representation of the given `code`.

  If the following bits are set, include the corresponding action in your list
  of commands, in order from lowest to highest.

  1 = wink
  10 = double blink
  100 = close your eyes
  1000 = jump

  10000 = Reverse the order of the operations in the secret handshake
  """
  @spec commands(code :: integer) :: list(String.t())
  def commands(code) do
    Enum.with_index(@key_list)
      |> Enum.map(fn(i) ->
        if band(code, 1 <<< elem(i, 1)) > 0 do elem(i, 0) end
      end)
      |> Enum.filter(&(&1 != nil))
      |> reverse(code)
  end

  def reverse(list, code) do
    if (code &&& 16) > 0 do
      Enum.reverse(list)
    else
      list
    end
  end
end

