def hey(statement):
    statement = statement.strip()
    if statement == '':
        return 'Fine. Be that way!'
    big_statement = statement.upper()
    all_alpha = any(c.isalpha() for c in statement)
    if statement == big_statement and all_alpha:
        return 'Whoa, chill out!'
    if statement[-1] == '?':
        return 'Sure.'
    return 'Whatever.'

