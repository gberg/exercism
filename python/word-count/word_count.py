def word_count(phrase):
    for ch in ['_',',',':']:
        if ch in phrase:
            phrase = phrase.replace(ch, ' ')
    words = phrase.split()
    word_count_map = {}
    for word in words:
        clean_word = ''
        clean_word = clean_word.join([i for i in word if i.isalpha() or i.isnumeric()])
        clean_word = clean_word.lower()
        if clean_word in word_count_map:
            word_count_map[clean_word] += 1
        else:
            word_count_map[clean_word] = 1
    return word_count_map

