def to_rna(dna_string):
    dna_map = {"G" : "C", "C" : "G", "T" : "A", "A" : "U"}
    rna_string = ''
    for n in dna_string:
        if n not in dna_map:
            return ''
        else: 
            rna_string += dna_map[n]
    return rna_string

