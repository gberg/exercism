def is_isogram(string):
    s = set()
    clean_string = '' 
    clean_string = clean_string.join([i for i in string if i.isalpha()])
    clean_string = clean_string.lower()
    for ch in clean_string:
        if ch in s:
            return False
        else:
            s.add(ch)
    return True
    
