def is_pangram(string):
    s = set()
    for letter in string:
        if letter.isalpha():
            s.add(letter.lower())
    return len(s) == 26

