module Pangram (isPangram) where
import Data.Char

isPangram :: String -> Bool
isPangram text = isSubset ['a'..'z'] (filter (/= ' ') (map toLower text))

isSubset :: Eq(a) => [a] -> [a] -> Bool
isSubset xs ys = all (\x -> elem x ys) xs
