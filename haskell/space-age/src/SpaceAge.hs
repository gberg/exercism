module SpaceAge (Planet(..), ageOn) where

data Planet = Mercury
            | Venus
            | Earth
            | Mars
            | Jupiter
            | Saturn
            | Uranus
            | Neptune
            deriving (Enum, Eq)

ageOn :: Planet -> Float -> Float
ageOn planet seconds
            | planet == Earth   = (earthAge seconds)
            | planet == Mercury = (earthAge seconds) / 0.2408467
            | planet == Venus   = (earthAge seconds) / 0.61519726
            | planet == Mars    = (earthAge seconds) / 1.8808158
            | planet == Jupiter = (earthAge seconds) / 11.862615
            | planet == Saturn  = (earthAge seconds) / 29.447498
            | planet == Uranus  = (earthAge seconds) / 84.016846
            | planet == Neptune = (earthAge seconds) / 164.79132
            | otherwise         = -1.0

earthAge :: Float -> Float
earthAge seconds = seconds / 31557600
