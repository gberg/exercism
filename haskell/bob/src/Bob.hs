module Bob (responseFor) where
import Data.Char 

responseFor :: String -> String
responseFor xs
            | isEmpty xs    = "Fine. Be that way!"
            | isYelling xs  = "Whoa, chill out!"
            | isQuestion xs = "Sure." 
            | otherwise     = "Whatever."

isYelling :: String -> Bool
isYelling input = all isUpper (removeNonAlpha input) && not (isEmpty (removeNonAlpha(input)))

removeNonAlpha :: String -> String
removeNonAlpha xs = filter isLetter xs

isQuestion :: String -> Bool
isQuestion input = head (dropWhile isSpace (reverse input)) == '?'

isEmpty :: String -> Bool
isEmpty input = all isSpace input 
