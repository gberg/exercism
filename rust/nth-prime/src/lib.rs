pub fn nth(n: u32) -> Option<u32> {
    if n == 0 {
        return None;
    }
    let nn = n as f64;
    let approx_end = ((nn * nn.ln()) + 100000f64) as usize;
    let mut filter: Vec<bool> = Vec::with_capacity(approx_end);
    for _i in 0..approx_end {
        filter.push(true);
    }
    let mut primes: Vec<usize> = Vec::with_capacity(approx_end);
    
    primes.push(2);
    let mut index = 3;
    while index < approx_end {
        if filter[index] {
            primes.push(index);
            let mut j = index;
            while j < approx_end {
                filter[j] = false;
                j = j + index;
            }
        }
        index = index + 2;
    }
    let pindex: usize = (n as usize) - 1;
    let x: Option<u32> = Some(primes[pindex] as u32);
    return x;
}
