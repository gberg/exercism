pub fn raindrops(n: usize) -> String {
    let mut retval = String::new();
    if n % 3 == 0 {
        retval.push_str("Pling")
    } 
    if n % 5 == 0 {
        retval.push_str("Plang")
    }
    if n % 7 == 0 {
        retval.push_str("Plong")
    }
    if retval.is_empty() {
        return format!("{}", n)
    } else {
        return retval
    }
}
